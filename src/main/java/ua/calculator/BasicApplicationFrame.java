package ua.calculator;

import javax.swing.*;
import java.awt.*;

public class BasicApplicationFrame {

    private final JFrame mainFrame = new JFrame();

    public BasicApplicationFrame() {
        mainFrame.setTitle("Basic Application v1.0.0");
        mainFrame.setBounds(100, 70, 300, 500);
        mainFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        mainFrame.setLayout(new BorderLayout());

        // top
        var top = new JPanel();
        top.setLayout(new BorderLayout());

        var inputField = new JTextField();
        inputField.setEditable(false);
        top.add(inputField, BorderLayout.CENTER);

        mainFrame.add(top, BorderLayout.NORTH);

        // bottom
        var bottom = new JPanel();
        bottom.setLayout(new GridLayout(5, 3));
        var calcBtn = new JButton("=");
        var btnOperandActionListener = new OperationButtonActionListener(inputField);
        var btnActionListener = new ButtonActionListener(inputField);

        var acBtn = new JButton("AC");
        acBtn.addActionListener(event ->{inputField.setText("");btnOperandActionListener.clear();});
        bottom.add(acBtn);

        var plusBtn = new JButton("+");
        plusBtn.addActionListener(btnOperandActionListener);
        bottom.add(plusBtn);

        var minusBtn = new JButton("-");
        minusBtn.addActionListener(btnOperandActionListener);
        bottom.add(minusBtn);
        var multiplicationBtn = new JButton("*");
        multiplicationBtn.addActionListener(btnOperandActionListener);
        bottom.add(multiplicationBtn);

        var divisionBtn = new JButton("/");
        divisionBtn.addActionListener(btnOperandActionListener);
        bottom.add(divisionBtn);

        var sqrtBtn = new JButton("\u221A");
        sqrtBtn.addActionListener(btnOperandActionListener);
        bottom.add(sqrtBtn);

        calcBtn.addActionListener(new EqualsButtonActionListener(btnOperandActionListener,inputField));
        bottom.add(calcBtn);

        for (int i = 0; i <= 9; i++) {
            var btn = new JButton(String.valueOf(i));
            btn.addActionListener(btnActionListener);
            bottom.add(btn);
        }

        mainFrame.add(bottom, BorderLayout.CENTER);
        mainFrame.setVisible(true);
    }
}