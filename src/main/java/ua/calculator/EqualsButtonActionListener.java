package ua.calculator;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class EqualsButtonActionListener implements ActionListener {
    private OperationButtonActionListener listener;
    private JTextField inputField;

    public EqualsButtonActionListener(OperationButtonActionListener listener, JTextField inputField) {
        this.listener = listener;
        this.inputField = inputField;
    }

    @Override
    public void actionPerformed(ActionEvent event) {
        String input = inputField.getText();
        if (input.contains("\u221A")) {
            input = input.substring(1);
        }
        String[] operands = input.split("\\D");
        ArrayList<String> signs = this.listener.getOperations();
        double result = Double.parseDouble(operands[0]);
        for (int i = 0; i < signs.size(); i++) {
            switch (signs.get(i)) {
                case "+": {
                    result += Double.parseDouble(operands[i + 1]);
                    break;
                }
                case "-": {
                    result -= Double.parseDouble(operands[i + 1]);
                    break;
                }
                case "*": {
                    result *= Double.parseDouble(operands[i + 1]);
                    break;
                }
                case "/": {
                    result /= Double.parseDouble(operands[i + 1]);
                    break;
                }
                case "\u221A": {
                    result = Math.sqrt(result);
                    break;
                }
                default: {
                    System.out.println("This operation is absent");
                }

            }

        }
        if (reconcilationResult(input, result)) {
            this.inputField.setText(String.valueOf(result));
        }
    }

    //**
    public boolean reconcilationResult(String input, double res) {
        boolean expResult = false;
        if (input.contains("\u221A")) {
            try {
                ScriptEngineManager scriptEngineManager = new ScriptEngineManager();
                ScriptEngine scriptEngine = scriptEngineManager.getEngineByName("Nashorn");
                expResult = Double.parseDouble(scriptEngine.eval(input).toString()) == res;
            } catch (ScriptException e) {
                e.printStackTrace();
            }
            System.out.println(expResult);
        } else {
            expResult = true;
        }
        return expResult;
    }

}
