package ua.calculator;

public final class CalculatorConstants {

    public static final String ERROR_MESSAGE = "ERR";

    private CalculatorConstants() {
    }
}