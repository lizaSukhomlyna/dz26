package ua.calculator;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import static ua.calculator.CalculatorConstants.ERROR_MESSAGE;


public class OperationButtonActionListener implements ActionListener {

    private ArrayList<String> operations = new ArrayList<>();
    private final JTextField input;

    public OperationButtonActionListener(JTextField input) {
        this.input = input;
    }

    public ArrayList<String> getOperations() {
        return operations;
    }
    public void clear(){
        this.operations=new ArrayList<>();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        var btn = (JButton) e.getSource();
        this.operations.add(btn.getText());
        if (input.getText().contains(ERROR_MESSAGE)) input.setText("");
        if(btn.getText()!="\u221A"){
        input.setText(input.getText() + btn.getText());}
        else {input.setText(btn.getText() + input.getText());}
    }

}